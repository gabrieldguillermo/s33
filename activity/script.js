
/*===Fetch ALL====*/
fetch("https://jsonplaceholder.typicode.com/todos")
 .then((response) => response.json())
 .then((json) => console.log(json));


/*====MAP=====*/
fetch("https://jsonplaceholder.typicode.com/todos")
.then((response) => response.json())

 .then((json) =>{
	json.map(function(todos){
	console.log(`id:${todos.id}, title : ${todos.title}  `);
	})
});

/*=======*/
 fetch("https://jsonplaceholder.typicode.com/todos/2")
  .then((response) => response.json())
 .then((json) => console.log(`title :${json.title} completed: ${json.completed}`));


/*====POST====*/
fetch("https://jsonplaceholder.typicode.com/todos",{
	method: "POST",
	headers: {
		"Content-Type" : "application/json"
	},
	body: JSON.stringify({
		title: "New to-do list",
		completed: true,
		userid:2
	})
})
.then((response) => response.json())
.then((json) => console.log(json))


/*====PUT===*/
fetch("https://jsonplaceholder.typicode.com/todos/2",{
	method: "PUT",
	headers: {
		"Content-Type" : "application/json"
	},
	body: JSON.stringify({
		title: "Update to-do list",
		description:"Changing the Data structure",
		completed: true,
		dateCompleted:" 09-06-2022",
		userid:10

	})
})
.then((response) => response.json())
.then((json) => console.log(json))


/*====PATCH====*/
fetch("https://jsonplaceholder.typicode.com/todos/1",{
	method: "PATCH",
	headers: {
		"Content-Type" : "application/json"
	},
	body: JSON.stringify({
		completed: true,
		dateCompleted: "09-02-2022"
	})
})
.then((response) => response.json())
.then((json) => console.log(json))
	


/*====DELETE===*/
fetch("https://jsonplaceholder.typicode.com/todos/10",{
	method: "DELETE"
})
